package com.venta.resource;

import com.venta.model.Venta;
import com.venta.service.VentaService;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
public class VentaResource {
    
    @Inject
    @Singleton
    VentaService service;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Venta postVenta(Venta venta){
        return service.addVenta(venta);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Venta> getVentas(){
        return service.listAllVentas();
    }
}
