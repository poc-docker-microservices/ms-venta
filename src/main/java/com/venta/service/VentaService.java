package com.venta.service;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.venta.model.Cliente;
import com.venta.model.Detalle;
import com.venta.model.Producto;
import com.venta.model.Venta;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class VentaService {
    
    @Inject
    MongoClient mongo;
    
    @ConfigProperty(name = "mongo.database") //Database name configured in application.properties
    String database;
    
    @ConfigProperty(name = "mongo.collection") //Database collection configured in application.properties
    String collection;
    
    public Venta addVenta(Venta venta){
        Bson bson;
        Document document = new Document()
            .append("fechaHoraVenta", venta.getFechaHoraVenta())
            .append("montoTotal", venta.getMontoTotal());
        Cliente cliente = venta.getCliente();
        
        Document clienteDocument = new Document()
            .append("_id", cliente.get_id().toString())
            .append("documentoIdentificacion", cliente.getDocumentoIdentificacion())
            .append("nombres", cliente.getNombres())
            .append("apPaterno", cliente.getApPaterno())
            .append("apMaterno", cliente.getApMaterno())
            .append("fechaNacimiento", cliente.getFechaNacimiento());
        document.append("cliente", clienteDocument);
        
        List<Detalle> detalles = venta.getDetalles();
        List<Document> detallesDocument = new ArrayList();
        for(Detalle detalle : detalles){
            Producto producto = detalle.getProducto();
            Document productoDocument = new Document()
                .append("_id", producto.get_id().toString())
                .append("producto", producto.getProducto())
                .append("valor", producto.getValor());
            Document detalleDocument = new Document()
                .append("producto", productoDocument)
                .append("cantidad", detalle.getCantidad());
            detallesDocument.add(detalleDocument);
        }
        
        document.append("detalles", detallesDocument);
        ObjectId insertedId = getCollection().insertOne(document).getInsertedId().asObjectId().getValue();
        System.out.println("Inserted Id: " + insertedId);
        venta.set_id(insertedId);
        return venta;
    }
    
    public List<Venta> listAllVentas(){
        MongoCursor<Document> cursor = getCollection().find().iterator();
        List<Venta> ventas = new ArrayList();
        
        while(cursor.hasNext()){
            Document document = cursor.next();
            Venta venta = new Venta();
            venta.setFechaHoraVenta(document.getDate("fechaHoraVenta").toInstant().atZone(ZoneId.of("UTC")).toLocalDateTime());
            venta.setMontoTotal(document.getInteger("montoTotal"));
            venta.set_id(document.getObjectId("_id"));
            
            Document documentCliente = (Document)document.get("cliente");
            Cliente cliente = new Cliente();
            cliente.setApMaterno(documentCliente.getString("apMaterno"));
            cliente.setApPaterno(documentCliente.getString("apPaterno"));
            cliente.set_id(new ObjectId(documentCliente.getString("_id")));
            cliente.setNombres(documentCliente.getString("nombres"));
            cliente.setDocumentoIdentificacion(documentCliente.getString("documentoIdentificacion"));
            cliente.setFechaNacimiento(documentCliente.getDate("fechaNacimiento").toInstant().atZone(ZoneId.of("UTC")).toLocalDate());
            venta.setCliente(cliente);
            
            List<Document> documentosDetalles = document.getList("detalles", Document.class);
            List<Detalle> detalles = new ArrayList();
            for(Document documentDetalle: documentosDetalles){
                
                Detalle detalle = new Detalle();
                detalle.setCantidad(documentDetalle.getInteger("cantidad"));
                   
                Document documentProducto = (Document)documentDetalle.get("producto");
                Producto producto = new Producto();
                producto.set_id(new ObjectId(documentProducto.getString("_id")));
                producto.setProducto(documentProducto.getString("producto"));
                producto.setValor(documentProducto.getInteger("valor"));
                detalle.setProducto(producto);
                detalles.add(detalle);
            }
            venta.setDetalles(detalles);
            
            ventas.add(venta);
        }
        
        return ventas;
    }
    
    private MongoCollection getCollection(){
        return mongo.getDatabase(database).getCollection(collection);
    }
}
