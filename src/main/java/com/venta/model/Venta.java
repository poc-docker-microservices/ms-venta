package com.venta.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.time.LocalDateTime;
import java.util.List;
import org.bson.types.ObjectId;

public class Venta {
    
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId _id;
    private LocalDateTime fechaHoraVenta;
    private Cliente cliente;
    private Integer montoTotal;
    private List<Detalle> detalles;

    public Venta() {
    }
    
    public Venta(ObjectId _id, LocalDateTime fechaHoraVenta, Cliente cliente, Integer montoTotal, List<Detalle> detalles) {
        this._id = _id;
        this.fechaHoraVenta = fechaHoraVenta;
        this.cliente = cliente;
        this.montoTotal = montoTotal;
        this.detalles = detalles;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public LocalDateTime getFechaHoraVenta() {
        return fechaHoraVenta;
    }

    public void setFechaHoraVenta(LocalDateTime fechaHoraVenta) {
        this.fechaHoraVenta = fechaHoraVenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(Integer montoTotal) {
        this.montoTotal = montoTotal;
    }

    public List<Detalle> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<Detalle> detalles) {
        this.detalles = detalles;
    }
    
    
    
}
